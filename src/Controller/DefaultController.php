<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * TODO: Add the double asterisk in the line above for this route to work
     * @Route("/", name="home")
     */

    public function index(Request $request)
    {
// Test... changing from https to ssh git set-url (2)

$time_start_1 = microtime(TRUE);

    for ($i=0; $i<1000; $i++) {
        $file = fopen(__DIR__, "r");
    }

$time_end_1 = microtime(TRUE);

$time_start_2 = microtime(TRUE);

     for ($i=0; $i<3; $i++) {
         $file = file_get_contents("http://www.google.com");
    }

$time_end_2 = microtime(TRUE);

$time_start_3 = microtime(TRUE);

for ($i=0; $i<10; $i++) {
            $a = array_fill(0, 1000, mt_rand(0, 1000));
            $b = array_fill(0, 1000, mt_rand(0, 1000));
            $c = [];
            foreach ($a as $h) {
                foreach ($b as $j) {
                    $c[] = $h*$j;
                }
            }
        }

$time_end_3 = microtime(TRUE);


$time_result_1 = $time_end_1 - $time_start_1;
$time_result_2 = $time_end_2 - $time_start_2;
$time_result_3 = $time_end_3 - $time_start_3;

	return new Response (nl2br("File system IO speed = . $time_result_1 \n 
				    Network speed =  . $time_result_2 \n
				    PHP execution speed =  . $time_result_3 "));
    }

}
